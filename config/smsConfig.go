package config

type SmsRecipientInfo struct {
	SendTo  []int64 `json:"send_to"`
	Content string  `json:"content"`
}

type SmsSenderInfo struct {
	ServiceType     string `json:"service_type"`
	Method          string `json:"method"`
	URL             string `json:"url"`
	Payload         interface{}
	Header          interface{}
	UniqueSmsID     string `json:"unique_sms_id"`
	CampaignName    string `json:"campaign_name"`
	Table           string `json:"table"`
	SmsDltContentId string `json:"sms_dlt_content_id"`
	SmsTemplateId   string `json:"sms_template_id"`
	SmsDltEntityId  string `json:"sms_dlt_entity_id"`
	SmsSender       string `json:"sms_sender"`
	SmsType         string `json:"sms_type"`
	AppCountry      string `json:"app_country"`
	CountryCode     string `json:"country_code"`
	DlrReq          string `json:"dlr_req"`
}

type SmsResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type SmsPostData struct {
	Ver      string     `json:"ver"`
	Key      string     `json:"key"`
	Encrpt   string     `json:"encrpt"`
	Messages []Messages `json:"messages"`
}

type Messages struct {
	Dest          []int64     `json:"dest"`
	Text          string      `json:"text"`
	Send          string      `json:"send"`
	Type          string      `json:"type"`
	DlrReq        string      `json:"dlr_req"`
	AppCountry    string      `json:"app_country"`
	CountryCd     string      `json:"country_cd"`
	Tag           interface{} `json:"tag"`
	Tag1          interface{} `json:"tag1"`
	Tag2          interface{} `json:"tag2"`
	DltEntityID   string      `json:"dlt_entity_id"`
	DltTemplateID interface{} `json:"dlt_template_id"`
}
