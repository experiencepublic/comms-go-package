package internal

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/experiencepublic/comms-go-package/config"
	"gitlab.com/experiencepublic/comms-go-package/pkg"
)

type SmsClient struct {
	SmsSenderInfo    *config.SmsSenderInfo
	SmsRecipientInfo *config.SmsRecipientInfo
	Comms            pkg.SmsCommunicator
}

func (s *SmsClient) SendSms() error {
	return s.Send()
}

/**
 * This will send SMS based on the service type
 */
func (s *SmsClient) Send() error {

	emailService := strings.ToLower(s.SmsSenderInfo.ServiceType)

	if emailService == "times" {
		return s.timesSmsService()

	}

	return nil
}

func (s *SmsClient) timesSmsService() error {

	var (
		messages    []config.Messages
		smsPostData config.SmsPostData
	)

	messages = append(messages, config.Messages{
		Dest:          s.SmsRecipientInfo.SendTo,
		Text:          s.SmsRecipientInfo.Content,
		Send:          s.SmsSenderInfo.SmsSender,
		Type:          s.SmsSenderInfo.SmsType,
		DlrReq:        s.SmsSenderInfo.DlrReq,
		AppCountry:    s.SmsSenderInfo.AppCountry,
		CountryCd:     s.SmsSenderInfo.CountryCode,
		Tag:           s.SmsSenderInfo.UniqueSmsID,
		Tag1:          s.SmsSenderInfo.CampaignName,
		Tag2:          s.SmsSenderInfo.Table,
		DltEntityID:   s.SmsSenderInfo.SmsDltEntityId,
		DltTemplateID: s.SmsSenderInfo.SmsDltContentId,
	})

	smsPostData.Ver = "1.0"
	smsPostData.Key = "cHVycGxsZV90cmFuczptNmk3bzlyfg=="
	smsPostData.Encrpt = "0"
	smsPostData.Messages = messages

	requestPayload, err := json.Marshal(smsPostData)

	if err != nil {
		return nil
	}
	payload := strings.NewReader(string(requestPayload))

	req, err := http.NewRequest("POST", "https://japi.instaalerts.zone/httpapi/JsonReceiver", payload)

	req.Header.Add("Content-Type", "application/json")

	response, _ := http.DefaultClient.Do(req)

	body, _ := ioutil.ReadAll(response.Body)

	defer response.Body.Close()

	if err != nil {
		return nil
	}

	fmt.Println(body)

	return nil
}
