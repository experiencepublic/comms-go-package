package comms

import (
	"gitlab.com/experiencepublic/comms-go-package/config"
	"gitlab.com/experiencepublic/comms-go-package/internal"
	"gitlab.com/experiencepublic/comms-go-package/pkg"
)

func NewEmailClient(emailConfig *config.SenderInfo, recipientInfo *config.RecipientInfo) *internal.EmailClient {

	client := &internal.EmailClient{
		SenderInfo:    emailConfig,
		RecipientInfo: recipientInfo,
	}

	return client
}

func GetEmailCommunicator(client *internal.EmailClient) *pkg.EmailService {

	emailComms := &pkg.EmailService{
		Comms:   client,
		Service: client,
	}

	return emailComms
}

func GetSenderConfig() *config.SenderInfo {

	return &config.SenderInfo{}
}

func GetRecipientInfo() *config.RecipientInfo {
	return &config.RecipientInfo{}
}
