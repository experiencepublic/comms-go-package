package comms

import (
	"gitlab.com/experiencepublic/comms-go-package/config"
	"gitlab.com/experiencepublic/comms-go-package/internal"
	"gitlab.com/experiencepublic/comms-go-package/pkg"
)

func NewSmsClient(smsConfig *config.SmsSenderInfo, recipientInfo *config.SmsRecipientInfo) *internal.SmsClient {

	client := &internal.SmsClient{
		SmsSenderInfo:    smsConfig,
		SmsRecipientInfo: recipientInfo,
	}

	return client
}

func GetSmsCommunicator(client *internal.SmsClient) *pkg.SmsCommunicator {

	smsComms := &pkg.SmsCommunicator{
		Comms: client,
	}

	return smsComms
}

func GetSmsSenderConfig() *config.SmsSenderInfo {

	return &config.SmsSenderInfo{}
}

func GetSmsRecipientInfo() *config.SmsRecipientInfo {
	return &config.SmsRecipientInfo{}
}
