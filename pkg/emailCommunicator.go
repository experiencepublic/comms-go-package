package pkg

type EmailCommunicator interface {
	SendMail() error
}

type EmailService struct {
	Comms   BaseCommunicator
	Service EmailCommunicator
}
