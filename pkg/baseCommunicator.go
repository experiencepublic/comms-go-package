package pkg

type BaseCommunicator interface {
	Send() error
}
